"use strict";

import { Character } from "./character.js";
import history from "../json/history.json";


let heroFromJson = localStorage.getItem('hero');
let parsedHero = JSON.parse(heroFromJson);

let hero = new Character(parsedHero);
console.log(hero);

let humanObj = {
  name: 'Enemy',
  hp: 8,
  equipment: {
    weapon: 'sword',
    armor: '',
    inventory: []
  },
  job: {
    description: 'enemy',
    agility: 2,
    intelligence: 2,
    strength: 2,
    diceValue: []
  }
};

let bossObj = {
  name: 'boss',
  hp: 8,
  equipment: {
    weapon: 'sword',
    armor: '',
    inventory: ['bossPo']
  },
  job: {
    description: 'enemy',
    agility: 2,
    intelligence: 2,
    strength: 2,
    diceValue: []
  }
};

let monsterObj = {
  name: 'monster',
  hp: 7,
  equipment: {
    weapon: '',
    armor: '',
    inventory: []
  },
  job: {
    description: 'monster',
    agility: 3,
    intelligence: 1,
    strength: 1,
    diceValue: []
  }
};

let humanEn = new Character(humanObj);
let monsterEn = new Character(monsterObj);
let boss = new Character(bossObj);

//Adding Text in div #text
function text(jsonFile, id) {
  let div = document.querySelector('#text');
  div.textContent = jsonFile['context'][id]['text'];

}

//Cleaning all HTML function
function clear() {
  let clearSection = document.querySelector('section');
  let clearDiv = document.querySelector('#text');
  let clearP = document.querySelectorAll('p');
  clearSection.remove();
  clearDiv.remove();
  clearP.innerHtml = '';

}

function combat(attacker, defender) {
  // div pour marquer les résultats des dés, à faire plus tard:
  // let divCombat = document.createElement('div');
  // document.body.appendChild(divCombat);

  let fightButton = document.createElement('button');
  fightButton.setAttribute('id', 'fight-button');
  fightButton.textContent = 'Fight!';
  document.body.appendChild(fightButton);

  let divResult = document.createElement('div');
  divResult.setAttribute('id', 'FightResult')
  document.body.appendChild(divResult);

  // button fight :
  fightButton.addEventListener('click', function () {

    attacker.job.attack();
    defender.job.defense();

    let attackerValue = attacker.job.diceValue.reduce((sum, val) => sum +=
      val);
    let defenderValue = defender.job.diceValue.reduce((sum, val) => sum += val);
    let array = [attackerValue, defenderValue];

    if (attacker.job.diceValue.reduce((sum, val) => sum + val) > defender.job.diceValue.reduce((sum, val) => sum + val)) {
      defender.hp -= defender.hp - attacker.damage;
      divResult.textContent = 'Good!';
    }
    else {
      divResult.textContent = 'Too bad';
    }

    let result = array;
    divResult.diceValue = result;
    console.log(result);

  });
  // return [attackerValue, defenderValue];
};




//Creating HTML contents function 
function structure(jsonFile, arrJson) {
  let section = document.createElement('section');
  let div = document.createElement('div');
  div.setAttribute('id', 'text');

  document.body.appendChild(section);
  section.appendChild(div);

  //loop which create paragraphs according to number of json choices + apply texts
  for (let index = 1; index <= arrJson.length; index++) {
    let p = document.createElement('p');
    section.appendChild(p);
    p.setAttribute('id', 'choice' + index);
    p.textContent = arrJson[index - 1].text;
    p.addEventListener('click', () => {
      //Search if combat 
      if (p.textContent.indexOf("---") != -1) {
        alert('Combat!');
        clear();
        combat(hero, humanEn);
        // structure(jsonFile, jsonFile['context'][arrJson[index - 1].target]['choices']);
        // text(jsonFile, arrJson[index - 1].target);
        structure(jsonFile, jsonFile['context'][arrJson[index - 1].target]['choices']);
        text(jsonFile, arrJson[index - 1].target);
      } else {
        clear();
        structure(jsonFile, jsonFile['context'][arrJson[index - 1].target]['choices']);
        text(jsonFile, arrJson[index - 1].target);
      }
    })

  }
}


structure(history, history['context']['0']['choices']);
text(history, ['0']);
