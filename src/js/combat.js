"use strict";



function confrontation(currentPlayer, difficulty) {
  let result = 0;
  let divResult = document.createElement('div');
  document.body.appendChild(divResult);
  currentPlayer.phyAction();
  currentPlayer.intAction();
  currentPlayer.agiAction();
  difficulty.valueDifficult();

  if (currentPlayer.diceValue.reduce((sum, val) => sum += val) > difficulty) {
    divResult.textContent = 'Good!';
  }
  else  {
    currentPlayer.hp -= currentPlayer.hp - difficulty.damage;
    divResult.textContent = 'Too bad'; //or 'Try again' ?
  }
};

