'use strict';

import { Character } from "./character";

let hero = new Character({ name: '', hp: '', equipment: '', job: '' });
let form = document.querySelector('#form-character');

form.addEventListener('submit', function (event) {

  event.preventDefault();

  let inputName = document.querySelector('#name').value;
  let inputJob = document.querySelector('.job:checked').value;
//transform  each variable in simple objects for local storage.
  let warr = {
    name: inputName,
    hp: 10,
    equipment: {
      weapon: 'sword',
      armor: '',
      inventory: []
    },
    job: {
      description: 'warrior',
      agility: 2,
      intelligence: 1,
      strength: 3,
      diceValue: []
    }
  };

  let wiz = {
    name: inputName,
    hp: 7,
    equipment: {
      weapon: 'staff',
      armor: '',
      inventory: []
    },
    job: {
      description: 'wizard',
      agility: 2,
      intelligence: 3,
      strength: 1,
      diceValue: []
    }
  };

  let arc = {
    name: inputName,
    hp: 8,
    equipment: {
      weapon: 'bow',
      armor: '',
      inventory: []
    },
    job: {
      description: 'archer',
      agility: 3,
      intelligence: 2,
      strength: 1,
      diceValue: []
    }
  };

  if (inputJob === "warrior") {
    hero = warr;
    console.log(hero);
  } else if (inputJob === "wizard") {
    hero = wiz;
    console.log(hero);
  } else if (inputJob === "archer") {
    hero = arc;
    console.log(hero);
  }

  let heroToJson = JSON.stringify(hero);
  console.log(heroToJson);

  localStorage.setItem('hero', heroToJson);
  window.location.href= './game.html';
})